package com.example.imagesendexample.network;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RetrofitService {
    // multipart 파일 업로드
    @Multipart
    @POST("SmartGlassRest/multipart_checklist_location")
    Call<ResponseBody> ImageUpload(
            @Part MultipartBody.Part image
    );
}
