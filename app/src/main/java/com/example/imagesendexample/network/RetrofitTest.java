package com.example.imagesendexample.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitTest {

    Retrofit retrofit;
    RetrofitService service;

    String BASE_URL = "https://koraildev.watttalk.kr:8401/korail/";


    public static RetrofitTest retrofitTest = new RetrofitTest();

    private RetrofitTest() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(RetrofitService.class);
    }

    public static RetrofitTest getInstance() {
        return retrofitTest;
    }

    public RetrofitService getService() {
        return service;
    }
}