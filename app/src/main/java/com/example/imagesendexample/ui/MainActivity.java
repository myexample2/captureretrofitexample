package com.example.imagesendexample.ui;

import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.imagesendexample.R;
import com.example.imagesendexample.network.RetrofitTest;
import com.example.imagesendexample.utils.AsyncTaskITF;
import com.example.imagesendexample.utils.Utils;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    // Pre fix 파일 이름
    private static final String PRE_FILE_NAME = "2022120154842460_foreground_66_";
    // 실제 저장 될 파일 이름
    private static String mFileName = "";
    // 통신 하기 위한 Header Key
    private static final String HEADER_KEY = "uploadFiles";

    // 캡쳐한 이미지 byte array
    public static byte[] mImageArray;

    // 카메라 관련 UI 및 객체
    private static CameraPreview surfaceView;
    private SurfaceHolder holder;
    private static Camera mCamera;
    private int mCameraStartFailCount = 0;  // 카메라 Start Fail Count
    // Capture 파일이름 인덱스
    // 서버로 이미지 전송 시 인덱스 증가
    private int mCaptureFileNameIndex = 0;
    // Activity 객체
    public static MainActivity mMainInstance;

    /**
     * UI
     **/
    private Button btnBack; // 뒤로가기 버튼
    private Button btnUpload; // 업로드 버튼
    private Button btnCapture; // 캡쳐 버튼

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 카메라 프리뷰를  전체화면으로 보여주기 위해 셋팅한다.
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        // 카메라 객체를 R.layout.activity_main의 레이아웃에 선언한 SurfaceView에서 먼저 정의해야 함으로 setContentView 보다 먼저 정의한다.
        mCamera = Camera.open();

        initUI();
    }

    /**
     * Init UI
     */
    private void initUI() {
        Log.d(TAG, "## initUI");
        mMainInstance = this;
        setContentView(R.layout.activity_main);

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(v -> {
            btnBack.setVisibility(View.GONE);
            btnUpload.setVisibility(View.GONE);

            btnCapture.setVisibility(View.VISIBLE);
            startPreview();
        });

        btnUpload = findViewById(R.id.btnUpload);
        btnUpload.setOnClickListener(v -> {
            btnBack.setVisibility(View.GONE);
            btnUpload.setVisibility(View.GONE);

            btnCapture.setVisibility(View.VISIBLE);
            startPreview();
            fileNameIndexAdded();
            Utils.mWriteToFile(mFileName, mImageArray, getApplicationContext());
            Utils.mAsyncTask(new AsyncTaskITF() {
                @Override
                public void doInBackground() {
                    try {
                        upload();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onPostExecute() {
                    // 작업 끝난 뒤 해당 Interface 실행
                }
            });
        });

        btnCapture = findViewById(R.id.btnCapture);
        btnCapture.setOnClickListener(v -> {
            btnBack.setVisibility(View.VISIBLE);
            btnUpload.setVisibility(View.VISIBLE);

            btnCapture.setVisibility(View.GONE);
            cameraCapture();
        });

        // SurfaceView를 상속받은 레이아웃을 정의한다.
        surfaceView = (CameraPreview) findViewById(R.id.preview);

        // SurfaceView 정의 - holder와 Callback을 정의한다.
        holder = surfaceView.getHolder();
        holder.addCallback(surfaceView);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    // Camera getter
    public static Camera getCamera() {
        return mCamera;
    }

    /**
     * 카메라 캡쳐 후 콜백 처리 함수
     */
    private void cameraCapture() {
        mCamera.takePicture(
                new Camera.ShutterCallback() {
                    @Override
                    public void onShutter() {

                    }
                },
                new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {

                    }
                },
                new Camera.PictureCallback() {
                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {
                        Log.d(TAG, "len : " + data.length);
                        mImageArray = new byte[data.length];
                        mImageArray = data;
                    }
                });
    }

    /**
     * 이미지 Multi part 전송
     * Retrofit2 라이브러리 사용
     *
     * @throws InterruptedException - 예외 내용
     */
    public void upload() throws InterruptedException {
        //  Note - 비동기 처리 테스트 하기 위한 sleep
//        Thread.sleep(2000);
        File imageFile = new File(getFilesDir() + "/" + mFileName); // 저장 경로

        RequestBody fileRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(HEADER_KEY, imageFile.getName(), fileRequestBody);
        Call<ResponseBody> call = RetrofitTest.getInstance().getService().ImageUpload(filePart);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "#onResponse");
                if (response.isSuccessful()) {
                    Log.d(TAG, "#response code : " + response.code());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "#onFailure");
            }
        });
    }

    /**
     * 파일 이름에 대한 인덱스 증가
     */
    private void fileNameIndexAdded() {
        StringBuilder sb = new StringBuilder(PRE_FILE_NAME);
        sb.append(String.valueOf(mCaptureFileNameIndex++)); // 파일 count 증가
        sb.append(".jpg");
        mFileName = sb.toString();
        Log.d(TAG, "mFileName : " + mFileName);
    }

    /**
     * Start Preview 함수
     * 예외처리 (실패 시 재귀 호출)
     * 실패 10번 이상 하는 경우 preview 중지
     */
    private void startPreview() {
        try {
            mCamera.startPreview();
        } catch (Exception e) {
            if (mCameraStartFailCount++ < 10) {
                try {
                    Thread.sleep(500);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
                startPreview();
                Toast.makeText(getBaseContext(), "Camera loding 중 잠시 기다려주세요.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getBaseContext(), "카메라 작동에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
        }
        mCameraStartFailCount = 0;
    }
}